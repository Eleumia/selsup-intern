import React, { ChangeEvent, FormEvent, useState } from "react";
import { nanoid } from "nanoid";

type Param = {
  id: string;
  name: string;
};

type ParamValue = {
  id: string;
  value: string;
};

type Model = {
  paramValues: ParamValue[];
};

type ParamEditorProps = {
  params: Param[];
  model: Model;
};

type ModelFormProps = {
  handleSubmit: (e: FormEvent) => void;
  modelData: Model;
  paramValueChangeHandler: (
    e: ChangeEvent<HTMLInputElement>,
    id: string
  ) => void;
  showModel: boolean;
  params: Param[];
};

const params: Param[] = [
  {
    id: nanoid(),
    name: "Назначение",
  },
  {
    id: nanoid(),
    name: "Длина",
  },
  {
    id: nanoid(),
    name: "Цвет",
  },
];

function ModelForm({
  handleSubmit,
  modelData,
  paramValueChangeHandler,
  showModel,
  params,
}: ModelFormProps) {
  return (
    <form className="form" onSubmit={handleSubmit}>
      {params.map((param, index) => {
        return (
          <div className="form__control" key={param.id}>
            <label>
              <span className="form__label-text">{param.name}</span>
              <input
                type="text"
                className="input"
                autoFocus={index === 0}
                value={
                  modelData.paramValues.find(
                    (paramValue) => paramValue.id === param.id
                  )?.value
                }
                onChange={(e) => paramValueChangeHandler(e, param.id)}
                name={param.name}
              />
            </label>
          </div>
        );
      })}

      <button className="button button--primary button--fullWidth">
        {showModel ? "Hide Model" : "Get Model"}
      </button>
    </form>
  );
}

function ModelTable({ modelData }: { modelData: Model }) {
  return (
    <table>
      <thead>
        <tr>
          <th>Param Id</th>
          <th>Param Value</th>
        </tr>
      </thead>
      <tbody>
        {modelData.paramValues.map((paramValue) => (
          <tr key={paramValue.id}>
            <td>{paramValue.id}</td>
            <td>{paramValue.value}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

function ParamEditor({ params, model }: ParamEditorProps) {
  const [modelData, setModelData] = useState<Model>(model);
  const [showModel, setShowModel] = useState(false);

  const paramValueChangeHandler = (
    e: ChangeEvent<HTMLInputElement>,
    id: string
  ) => {
    const { value } = e.target;
    const paramValueIndex = modelData.paramValues.findIndex(
      (paramValue) => paramValue.id === id
    );

    if (paramValueIndex !== -1) {
      const newParamValues = modelData.paramValues.map((paramValue, index) => {
        return index === paramValueIndex
          ? { ...paramValue, value }
          : paramValue;
      });
      setModelData({
        paramValues: newParamValues,
      });
    }
  };

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    setShowModel(!showModel);
  };

  return (
    <div className="container">
      <ModelForm
        handleSubmit={handleSubmit}
        modelData={modelData}
        paramValueChangeHandler={paramValueChangeHandler}
        showModel={showModel}
        params={params}
      />
      {showModel && <ModelTable modelData={modelData} />}
    </div>
  );
}

function App() {
  const model = {
    paramValues: params.map((param) => ({ id: param.id, value: "" })),
  };

  return <ParamEditor model={model} params={params} />;
}

export default App;
